# DroidKt

Part of the devops process is having a reliable build setup that allows continous buillds 
and this is it. Its also part of my effort to make porting to FuchsiaOS native 
dart apps painless from android kotlin.

## Features

Regression Testing

instrumented TDD Testing

BDD user Case testing

Gitlab CI Continous build Setup

Kotlin tools and dependencies

Gradle tricks to integrate dep updates via Android Studio project Structures Dailoog


## Port To FuchsiaOS Dart Apps premise

If one aligns kotlin android app dev patterns the right way, one can repurpose the 
cross flutter sdk to port classic android kotlin apps to FuchsiaOS native 
dart.

Kotlin Coroutines Including Channels

Anko Reactive Layouts without XML

Google Binding 

Google MVVM patterns(Can also do flux-redux patterns but keeping Google way for now to show how paimless it is)


Pure and SideEffect Monoids


Notice one does not see RxJava? We actually can fully get rid of using rxJava.



## Tech Details

This more directed at startup co-founder ctos and other devs as it has some more tech 
detail to it.

###  Project Structure

Some plugins still do not handle multi-modules right and since Flutter cross platfrom apps 
can only have one module of app all separation of developer concenrs is done via 
different packages rather than different project modules.

### API Level Targeting

Due to TLS being updated to 1.2 and browsers refusing to connect to TLS1.1 past fall 2019 
and Google only accepting apps targeting 8.1 and higher in PlayStore past Nov 2019 this 
build setup targets api level 27 and up which os 8.1 ie Oreo and up. Okhttp deps 
are updated to the java8 tLS 1.2 support version.


### Debug

I use a combination of HugoTemp(hugo code correction for kotlin) DebugLog annotation, 
LeakCanary tooling to detect leaks, and Madge and Scalpel to setup a debug drawer 
to debug images and visual parts of the UI along with setting up a Timberkt Debug tree 
to automatically prevent log calls from going into production code.

[Hugo](https://github.com/JakeWharton/hugo)

[LeakCanary](https://github.com/square/leakcanary)

[Madge](https://github.com/JakeWharton/madge)

[Scalpel](https://github.com/JakeWharton/scalpel)

[Timberkt](https://github.com/ajalt/timberkt)

### App Versioning

I use Jake Wharton's lazy way of updating app version via a 3rd party 
Gradle Grgit plugin to read git stuff opn MS Windows correctly.


### Auto Deps Updating


Gradle trick picked up from Google Engineers, versions.gradle file desp are specified 
in a pattern that Android Studio 3.5 understnads so that can auto update 
all the deps via the Project Strucutres Menu commmand and dialog.

### Testing

Using Junit5 testing via 3rd party plugin as the gradle junit5 tooling does not work with the 
Google Android gradle plugin.

[Androidjunit5](https://github.com/mannodermaus/android-junit5)

Paramterized testing directions are here

https://junit.org/junit5/docs/current/user-guide/#writing-tests-parameterized-tests

Mockito set up with jetbrains open plugin to mock final 
calls faster than mockito's own solution and a kotlin-mockito helper is used.

[MockitoKotlin](https://github.com/nhaarman/mockito-kotlin)

[MockitoWiki](https://github.com/mockito/mockito/wiki)



#### Regression Testing

via Pitest

[Pitest](http://pitest.org/)

[GradlePitestPlugin](https://github.com/koral--/gradle-pitest-plugin)

### Instrumented TDD 

Via either Spoon or Espresso tooling for both local device testing or cloud based testing
Also use a test dal that combines Espress and UIAutomator calls

[AndroidTestKtx](https://github.com/codecentric/androidtestktx)

### BDD

using Spek framework with Mockito. 

[Spek](https://spekframework.org/core-concepts/)


### Gitlab CI Runner Setup

Due to docker privelage mode being a security violation on shared runners and the only way to 
get access to KVM for gpu acceleration for the emulator I only run regression and bdd tests 
on the gitlab shared runner setup and generally that is enough as I am alwys running 
instrumented TDD tests locally.