/* Copyright 2019 Fred Grott

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package io.gitlab.fredgrott.droidkt

import android.app.Application
import android.content.Context
import android.os.Build
import android.util.Log
import android.util.Log.INFO
import androidx.multidex.MultiDex
import com.github.ajalt.timberkt.Timber
import com.github.ajalt.timberkt.Timber.DebugTree
import com.squareup.leakcanary.LeakCanary
import android.os.StrictMode



class MyApp : Application() {

    val Context.myApp: MyApp
        get() = applicationContext as MyApp

    override fun onCreate() {
        super.onCreate()
        // see https://fabric.io/kits/android/crashlytics/install
        // Fabric.with(this, new CrashAnalytics())
        // }

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
            //set strict mode as most compat libs for this are not kept uptodate so doing this manually
            // note this does not detect the android 9 level nonsdk calls warngin stuff
            StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()   // or .detectAll() for all detectable problems
                    .penaltyLog()
                    .build()
            )
            StrictMode.setVmPolicy(
                StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build()
            )
            
        } else {
            Timber.plant(CrashReportingTree())
        }

       if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
         }
         LeakCanary.install(this)
    }
    override fun attachBaseContext(base: Context) {

        super.attachBaseContext(base )
        MultiDex.install(this)
    }
    private class CrashReportingTree : timber.log.Timber.Tree() {
        override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        }

        fun isLoggable(priority: Int, tag: String?): Boolean {
            return priority >= INFO
        }

        protected fun log(priority: Int, tag: String, t: Throwable?, message: String) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return
            }

            FakeCrashLibrary.log(priority, tag, message)

            if (t != null) {
                if (priority == Log.ERROR) {
                    FakeCrashLibrary.logError(t)
                } else if (priority == Log.WARN) {
                    FakeCrashLibrary.logWarning(t)
                }
            }
        }
    }
}