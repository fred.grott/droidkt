/* Copyright 2019 Fred Grott

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package io.gitlab.fredgrott.droidkt

import android.app.Activity
import android.media.audiofx.DynamicsProcessing
import com.github.ajalt.timberkt.Timber.i

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import com.jraska.falcon.FalconSpoonRule




object Utils {

    fun getCurrentActivity(): Activity {
        val currentActivity = arrayOf<Activity>()
        InstrumentationRegistry.getInstrumentation().runOnMainSync(Runnable {
            val resumedActivity = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED)
            val it = resumedActivity.iterator()
            currentActivity[0] = it.next()
        })

        return currentActivity[0]
    }

    fun screenShot(rule: FalconSpoonRule, tag: String) {
        rule.screenshot(getCurrentActivity(), tag)

        i { "log screenshot taken" }

    }

}